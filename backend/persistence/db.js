const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  "mysql://entremares:entremares@entremaresdb.ccuhbkuvnq7i.us-east-1.rds.amazonaws.com/entremaresdb"
);

const bcrypt = require("bcrypt");

const Property = require("../models/Property")(Sequelize, sequelize);
const Customer = require("../models/Customer")(Sequelize, sequelize);
const Reservation = require("../models/Reservation")(Sequelize, sequelize);

const User = require("../models/User")(Sequelize, sequelize);

const Image = require("../models/Image")(Sequelize, sequelize);
const Counter = require("../models/Counter")(Sequelize, sequelize);

/*
const Category = require("../models/Category")(
  Sequelize,
  sequelize,
  Establishment
);


const Waitress = require("../models/Waitress")(Sequelize, sequelize, User);

const Product = require("../models/Product")(
  Sequelize,
  sequelize,
  Establishment,
  Category
);

const Discount = require("../models/Discount")(Sequelize, sequelize, Product);

const StockItem = require("../models/StockItem")(Sequelize, sequelize, User);
const Stock = require("../models/Stock")(Sequelize, sequelize, User, StockItem);

const Image = require("../models/Image")(Sequelize, sequelize, Product);

const CreditCard = require("../models/CreditCard")(Sequelize, sequelize);
const Account = require("../models/Account")(Sequelize, sequelize, User);
const Payment = require("../models/Payment")(Sequelize, sequelize, User);
*/
//const OrderItem = require("../models/OrderItem")(Sequelize, sequelize);

/*const { Order, OrderItem } = require("../models/Order")(
  Sequelize,
  sequelize,
  User,
  Waitress,
  Payment
);

const SMSStatus = require("../models/SMSStatus")(Sequelize, sequelize);
*/
// sync wuth database
sequelize.sync();

// initialize data
init_db = async () => {
  var user = await User.findOne({ where: { login: "admin" } });
  if (user == null) {
    var admin = new User();

    admin.login = "admin";
    admin.password = bcrypt.hashSync("kiwkiwrocks", 1);

    admin.save();
  }
};

// init_db();

module.exports = {
  Counter,
  Reservation,
  Customer,
  User,
  Property,
  Image
};
