// server libs
const Hapi = require("hapi");
const Inert = require("inert");
const Vision = require("vision");
const Joi = require("joi");
const HapiSwagger = require("hapi-swagger");

// encrypts
const bcrypt = require("bcrypt");
const validateToken = require("./auth/myjwt");

// endpoints
const user = require("./endpoints/user");
const product = require("./endpoints/product");
const establishment = require("./endpoints/establishment");
const order = require("./endpoints/order");
const payment = require("./endpoints/payment");
const sms = require("./endpoints/sms");
const stock = require("./endpoints/stock");

const customer = require("./endpoints/customer");
const reservation = require("./endpoints/reservation");

// api
const server = Hapi.Server({
  host: "0.0.0.0",
  port: 8081,
  routes: {
    cors: true
  }
});

const options = {
  basePath: "/v1",
  info: {
    title: "Entre Mares API documentation",
    description: "REST API for Entre Mares app",
    version: "0.0.1"
  },
  grouping: "tags",
  tags: [{
      name: "user",
      description: "User operations"
    },
    {
      name: "waitress",
      description: "Waitress operations"
    },
    {
      name: "product",
      description: "Product operations"
    },
    {
      name: "order",
      description: "Order operations"
    },
    {
      name: "payment",
      description: "Payment operations"
    },
    {
      name: "sms",
      description: "SMS operations"
    },
    {
      name: "qr",
      description: "QRCode operations"
    },
    {
      name: "property",
      description: "Property operations"
    },
    {
      name: "customer",
      description: "Customer operations"
    },
    {
      name: "reservation",
      description: "Reservation operations"
    }


  ]
};

var routes = [].concat(user, establishment, customer, reservation);

const startServer = async() => {
  // setting plugins
  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: options
    }
  ]);

  // setting jwt - json web token
  await server.register(require("hapi-auth-jwt2"));

  server.auth.strategy("jwt", "jwt", {
    key: "kiwkiwrocks",
    validate: validateToken,
    verifyOptions: { algorithms: ["HS256"] }
  });

  // setting routes
  server.realm.modifiers.route.prefix = "/v1";
  await server.route(routes);

  // start server
  await server.start();

  console.log(`Server is running at ${server.info.uri}`);

  process.on("unhandledRejection", err => {
    console.log(err);
  });
};

startServer();
