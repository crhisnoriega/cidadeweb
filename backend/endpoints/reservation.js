const fs = require("fs");

const Joi = require("joi");
const { Reservation, Customer, Property, Image } = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [{
    path: "/reservation",
    method: "POST",
    config: {
      handler: async function (req, res) {
        console.log(req.payload);

        if (req.payload.id == null) {
          req.payload.id = uuidv1();
        }

        req.payload.customer_code = '12d59350-b49e-11e9-927f-99b21f7dd04e';
        req.payload.status = 'initialize';
        console.log(req.payload);



        try {
          var reservation = await Reservation.create(req.payload);
          reservation.save();

          var property = await Property.findOne(req.payload, {
            where: { id: req.payload.property_code }
          });

          property.status = "disable";
          property.save();

          return res.response(reservation).code(200);
        } catch (e) {
          console.log("error");
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register Reservation",
      notes: "Register Reservation",
      tags: ["api", "reservation"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/reservation/{reservationid}",
    method: "PUT",
    config: {
      handler: async function (req, res) {
        console.log(req.payload);

        try {
          var reservation = await Reservation.update(req.payload, {
            where: { id: req.params.reservationid }
          });

          return res.response(reservation);
        } catch (e) {
          console.log("error");
          console.log(JSON.stringify(e));
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Update Reservation",
      notes: "Update Reservation",
      tags: ["api", "reservation"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          reservationid: Joi.string()
            .required()
            .description("reservation id")
        },
        payload: {}
      }
    }
  },
  {
    path: "/reservation/{reservationid}",
    method: "GET",
    config: {
      handler: async function (req, res) {
        console.log(req.payload);

        try {
          var reservation = await Reservation.findOne({
            where: { id: req.params.reservationid }
          });

          var customer = {}
          var property = {}

          if (reservation) {
            var custom = await Customer.findOne({
              where: { id: reservation.customer_code }
            });
            if (custom) {
              customer = { value: custom.codigo, label: custom.codigo + " - " + custom.name };
            }


            var prop1 = await Property.findOne({
              where: { id: reservation.property_code }
            });

            if (prop1) {
              property = { value: prop1.codigo, label: prop1.codigo + " - " + prop1.name };
            }
          }

          return res.response({ reservation, customer, property });
        } catch (e) {
          console.log("error");
          console.log(JSON.stringify(e));
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Retrieve Reservation",
      notes: "Retrieve Reservation",
      tags: ["api", "reservation"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          reservationid: Joi.string()
            .required()
            .description("reservation id")
        }
      }
    }
  },
  {
    path: "/reservation/search",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          var reservations = await Reservation.findAll({
            order: [
              ["createdAt", "DESC"]
            ]
          });

          return res.response(reservations);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Search Reservation info",
      notes: "Search Reservation info",
      tags: ["api", "reservation"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query")
        }
      }
    }
  },
  {
    path: "/reservation/search/complete",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          var reservations = await Reservation.findAll({
            order: [
              ["createdAt", "DESC"]
            ]
          });


          var complete_result = [];

          for (var index = 0; index < reservations.length; index++) {
            var reservation = reservations[index];
            var customer = await Customer.findOne({
              where: { id: reservation.customer_code }
            });


            var property = await Property.findOne({
              where: { id: reservation.property_code }
            });



            complete_result.push({ reservation: reservation, customer: { name: customer.name }, property: { name: property.name } })
          }


          return res.response(complete_result);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Search Reservation info",
      notes: "Search Reservation info",
      tags: ["api", "reservation"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query")
        }
      }
    }
  },

  {
    path: "/reservation/status/{reservationid}",
    method: "PUT",
    config: {
      handler: async function (req, res) {
        console.log("put reservation")
        console.log(req.payload);

        try {
          var reservation = await Reservation.findOne({
            where: { id: req.params.reservationid }
          });

          reservation.status = req.payload.status.value;

          reservation.save();

          return res.response(reservation);
        } catch (e) {
          console.log("error");
          console.log(JSON.stringify(e));
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Retrieve Reservation",
      notes: "Retrieve Reservation",
      tags: ["api", "reservation"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          reservationid: Joi.string()
            .required()
            .description("reservation id")
        }
      }
    }
  }
];
