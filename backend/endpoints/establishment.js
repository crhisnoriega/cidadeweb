const fs = require("fs");

const Joi = require("joi");
const { Counter, Property, Image } = require("../persistence/db");
const { uploadS3FileSync, uploadS3Base64Async } = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [{
    path: "/properties",
    method: "POST",
    config: {
      handler: async function (req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            console.log("making insert");
            req.payload.id = uuidv1();

            var property = await Property.create(req.payload);
            property.save();

            return res.response(property).code(200);
          } else {
            console.log("making update");
            var property = await Property.update(req.payload, {
              where: { id: req.payload.id }
            });

            return res.response(property).code(200);
          }
        } catch (e) {
          console.log("error");
          console.log(JSON.stringify(e));
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register Property",
      notes: "Register Property",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/properties/{propertyid}",
    method: "PUT",
    config: {
      handler: async function (req, res) {
        console.log(req.payload);

        try {
          var property = await Property.update(req.payload, {
            where: { id: req.params.propertyid }
          });

          return res.response(property);
        } catch (e) {
          console.log("error");
          console.log(JSON.stringify(e));
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register Property",
      notes: "Register Property",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          propertyid: Joi.string()
            .required()
            .description("base64 endode")
        },
        payload: {}
      }
    }
  },
  {
    path: "/properties/{propertyid}",
    method: "GET",
    config: {
      handler: async function (req, res) {
        console.log(req.payload);

        try {
          var property = await Property.findOne({
            where: { id: req.params.propertyid }
          });
          return res.response(property);
        } catch (e) {
          console.log("error");
          console.log(JSON.stringify(e));
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register Property",
      notes: "Register Property",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          propertyid: Joi.string()
            .required()
            .description("base64 endode")
        }
      }
    }
  },
  {
    path: "/properties/search",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          var properties = await Property.findAll({
            where: {
              [Op.or]: [{
                  name: {
                    [Op.like]: "%" + req.query.query + "%"
                  }
                },
                {
                  codigo: {
                    [Op.like]: "%" + req.query.query + "%"
                  }
                }
              ]
            },
            order: [
              ["createdAt", "DESC"]
            ]
          });

          for (var i = 0; i < properties.length; i++) {
            var property = properties[i];
            var image = await Image.findOne({
              where: { propertyid: property.id }
            });

            if (image != null) {
              property.img = image.base64;
            }
          }

          return res.response(properties);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query Property info",
      notes: "Query for Property info",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query")
        }
      }
    }
  },
  {
    method: "POST",
    path: "/properties/uploadimage/{propertyid}",
    config: {
      payload: {
        output: "stream",
        allow: "multipart/form-data" // important
      },
      handler: async function (req, res) {
        var file = req.payload.file;

        console.log("upload image");
        console.log(file.filename);

        var property = await Property.findOne({
          where: { id: req.params.propertyid }
        });

        const filename = file.hapi.filename;
        const data = file._data;

        fs.writeFileSync(filename, data);
        uploadS3FileSync(filename, filename, property);
        return res.response({ status: "OK" }).code(200);
      },
      description: "Add image to property",
      notes: "property",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          propertyid: Joi.string()
            .required()
            .description("base64 endode")
        },
        payload: {}
      }
    }
  },
  {
    method: "POST",
    path: "/properties/base64image/{propertyid}",
    config: {
      payload: {
        timeout: false,
        maxBytes: 50000000
      },

      handler: async function (req, res) {
        console.log(req.payload);

        var file = req.payload.file;

        if (req.payload.id == null) {
          req.payload.id = uuidv1();
        }

        try {
          uploadS3Base64Async(req.payload.base64, req.payload.name, req.params.propertyid);
        } catch (e) {
          console.log(e);
        }

        try {
          var image = await Image.create(req.payload);

          image.propertyid = req.params.propertyid;

          image.save();

          return res.response({ status: "OK" }).code(200);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Add image to property",
      notes: "property",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          propertyid: Joi.string()
            .required()
            .description("base64 endode")
        },
        payload: {}
      }
    }
  },
  {
    path: "/properties/images/{propertyid}",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          console.log("find images for: " + req.query.propertyid);
          var images = await Image.findAll({
            where: {
              propertyid: {
                [Op.like]: "%" + req.params.propertyid + "%"
              }
            },

            order: [
              ["createdAt", "DESC"]
            ]
          });

          return res.response(images);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query Images",
      notes: "Query for Images",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        }
      }
    }
  },

  {
    path: "/properties/s3_images/{propertyid}",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          console.log("find images for: " + req.query.propertyid);
          var images = await Image.findAll({
            where: {
              [Op.and]: [{
                propertyid: {
                  [Op.like]: "%" + req.params.propertyid + "%"
                }
              }, {
                url: {
                  [Op.ne]: null
                }
              }]
            },

            order: [
              ["createdAt", "DESC"]
            ]
          });

          return res.response(images);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query Images",
      notes: "Query for Images",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          propertyid: Joi.string()
            .required()
            .description("property id")
        }
      }
    }
  },

  {
    path: "/properties/destinations",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          const destiny_list = [
            { value: "praias", label: "Praia" },
            { value: "aguas", label: "Aguas Termais" },
            { value: "serra", label: "Serra" },
            { value: "pescaria", label: "Pescaria" },
            { value: "convenios", label: "Convenios" }
          ];
          return res.response(destiny_list);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query destination info",
      notes: "Query for destination info",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        }
      }
    }
  },

  {
    path: "/properties/categories/{destinationid}",
    method: "GET",
    config: {
      handler: async function (req, res) {
        console.log(req.params.destinationid);
        try {
          var table = {
            praias: [
              { value: "nordeste", label: "Litoral Nordestino" },
              { value: "paulista", label: "Litoral Paulista" },
              { value: "carioca", label: "Litoral Carioca" },
              { value: "parananese", label: "Litoral Paranaense" },
              { value: "catarinense", label: "Litoral Catarinense" }
            ]
          };

          return res.response(table[req.params.destinationid]);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query category info",
      notes: "Query for category info",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },

        params: {
          destinationid: Joi.string()
            .required()
            .description("Destination id")
        }
      }
    }
  },

  {
    path: "/properties/subcategories/{categoryid}",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          var table = {
            nordeste: [{ value: "salvador", label: "Salvador - BA" }],
            paulista: [{ value: "praiagrande", label: "Praia Grande – SP" }]
          };

          return res.response(
            table[req.params.categoryid] ? table[req.params.categoryid] : []
          );
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query sub-category info",
      notes: "Query for sub-category info",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },

        params: {
          categoryid: Joi.string()
            .required()
            .description("Destination id")
        }
      }
    }
  },

  {
    path: "/properties/counter",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          var counter = await Counter.findOne({
            where: { id: "property_counter" }
          });

          if (counter == null || counter == []) {
            counter = new Counter();
            counter.id = "property_counter";
            Counter.create(counter);
          }

          counter.counter = counter.counter + 1;

          counter.save();

          return res.response(counter);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query sub-category info",
      notes: "Query for sub-category info",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        }
      }
    }
  },
  {
    path: "/properties/filter_search",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          console.log(req.query);


          var properties = await Property.findAll({

          });

          for (var i = 0; i < properties.length; i++) {
            var property = properties[i];
            var image = await Image.findOne({
              where: { propertyid: property.id }
            });

            if (image != null) {
              property.img = image.base64;
            }
          }

          return res.response(properties);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query Property info",
      notes: "Query for Property info",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          data_ini: Joi.date()
            .required()
            .description("data inicio"),

          data_fim: Joi.date()
            .required()
            .description("data fim"),

          estado: Joi.string()
            .description("estado"),

          city: Joi.string()
            .description("cidade"),

          capacity: Joi.string()
            .description("capacidade"),

          destinationid: Joi.string()
            .description("destino"),

          categoryid: Joi.string()
            .description("categoria"),



        }
      }
    }
  },


  {
    path: "/properties/filter_search_web",
    method: "GET",
    config: {
      handler: async function (req, res) {
        try {
          console.log(req.query);

          const offset = req.query.page * req.query.pageSize
          const limit = offset + req.query.pageSize

          if (req.query.city == null) req.query.city = "%%"
          if (req.query.estado == null) req.query.estado = "%%"
          if (req.query.destinationid == null) req.query.destinationid = "%%"
          if (req.query.categoryid == null) req.query.categoryid = "%%"
          if (req.query.capacity == null) req.query.capacity = -1


          console.log(req.query);

          var properties = await Property.findAll({
            limit,
            offset,
            where: {
              [Op.and]: [
                {
                  city: {
                    [Op.like]: "%" + req.query.city + "%"
                  }
                },
                {
                  state: {
                    [Op.like]: "%" + req.query.estado + "%"
                  }
                },
                {
                  destiny: {
                    [Op.like]: "%" + req.query.destinationid + "%"
                  }
                },
                {
                  subcategory: {
                    [Op.like]: "%" + req.query.categoryid + "%"
                  }
                },

                {
                  capacity: {
                    [Op.gte]: req.query.capacity
                  }
                }
              ]
            },
            order: [["createdAt", "DESC"]]
          });

          for (var i = 0; i < properties.length; i++) {
            var property = properties[i];
            var image = await Image.findOne({
              where: { propertyid: property.id }
            });

            if (image != null) {
              property.img = image.base64;
            }
          }

          return res.response(properties);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query Property info",
      notes: "Query for Property info",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {

          estado: Joi.string()
            .description("estado"),

          city: Joi.string()
            .description("cidade"),

          capacity: Joi.string()
            .description("capacidade"),

          destinationid: Joi.string()
            .description("destino"),

          categoryid: Joi.string()
            .description("categoria"),

          page: Joi.number()
            .description("numero da pagina"),

          pageSize: Joi.number()
            .description("tamanho da pagina"),
        }
      }
    }
  }
];
