const fs = require("fs");

const Joi = require("joi");
const { Order, User, CreditCard, Payment } = require("../persistence/db");
const { doPurchase, cancelPurchase } = require("../payment/cielopayment");

const qr = require("qr-image");

module.exports = [
  {
    path: "/stock/retrieve",
    method: "POST",
    config: {
      handler: async function(req, res) {},
      description: "Retrieve products",
      notes: "Retrieve ",
      tags: ["api", "stock"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          orderid: Joi.string().required(),
          creditcardid: Joi.string().required()
        }
      }
    }
  },
  {
    path: "/stock/qr",
    method: "GET",
    config: {
      handler: async function(req, res) {
        var order = await Order.findOne({ where: { id: req.query.orderid } });

        if (order != null) {
          var qr_png = qr.image(JSON.stringify(order), { type: "png" });
          return res.response(qr_png).type("image/png");
        } else {
          return res.response({ error: "order dont fount" });
        }
      },
      description: "Generate QR Code for retrieve",
      notes: "QR operations",
      tags: ["api", "stock", "qr"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          orderid: Joi.string()
            .required()
            .description("order id")
        }
      }
    }
  }
];
