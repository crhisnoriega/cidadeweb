const fs = require("fs");

const Joi = require("joi");
const {
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

module.exports = [
  {
    path: "/product",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var establish = await Establishment.findOne({
            where: { id: req.payload.establishmentid }
          });

          var cat = await Category.findOne({
            where: { id: req.payload.categoryid }
          });

          var product = await Product.create(req.payload);

          product.setEstablishment(establish);
          product.setCategory(cat);
          product.save();

          return res.response(product);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register product",
      notes: "Product",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          establishmentid: Joi.string()
            .required()
            .description("establishment id"),
          categoryid: Joi.string()
            .required()
            .description("category id"),
          name: Joi.string()
            .required()
            .description("product  name"),
          unit: Joi.string()
            .required()
            .description("unit"),
          unitPrice: Joi.string()
            .required()
            .description("price unit")
        }
      }
    }
  },
  {
    method: "POST",
    path: "/product/uploadimage/{productid}",
    config: {
      payload: {
        output: "stream",
        allow: "multipart/form-data" // important
      },
      handler: async function(req, res) {
        var file = req.payload.file;

        console.log("upload image");
        console.log(file.filename);

        var product = await Product.findOne({
          where: { id: req.params.productid }
        });

        const filename = file.hapi.filename;
        const data = file._data;

        fs.writeFileSync(filename, data);
        uploadS3FileSync(filename, filename, product);
        return res.response({ status: "OK" }).code(200);
      },
      description: "Add image to product",
      notes: "Product",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          productid: Joi.string()
            .required()
            .description("base64 endode")
        },
        payload: {}
      }
    }
  },
  {
    method: "PUT",
    path: "/product/image/{productid}",
    config: {
      handler: async function(req, res) {
        var product = await Product.findOne({
          where: { id: req.params.productid }
        });

        if (product != null) {
          var base64Image = req.payload.base64.split(";base64,").pop();
          fs.writeFileSync(req.payload.filename, base64Image, {
            encoding: "base64"
          });
          uploadS3FileSync(req.payload.filename, req.payload.filename, product);
          return res.response({ status: "OK" }).code(200);
        } else {
          return res.response({ status: "product not found" }).code(501);
        }
      },
      description: "Add image to product",
      notes: "Product",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          productid: Joi.string()
            .required()
            .description("base64 endode")
        },
        payload: {
          base64: Joi.string()
            .required()
            .description("base64 endode"),
          filename: Joi.string()
            .required()
            .description("file name")
        }
      }
    }
  },
  {
    path: "/product/{productid}",
    method: "GET",
    config: {
      handler: async function(req, res) {
        var products = await Product.findAll({
          where: { id: req.params.productid }
        });

        var result = [];

        for (p in products) {
          console.log(products[p].id);
          var images = await Image.findAll({
            where: { productId: req.params.productid }
          });
          result.push({ product: products[p], images: images });
        }

        return res.response(result);
      },
      description: "Retrieve product info",
      notes: "product info",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          productid: Joi.string()
            .required()
            .description("product id")
        }
      }
    }
  },
  {
    path: "/product/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var products = await Product.findAll({
            where: {
              name: {
                [Op.like]: "%" + req.query.query + "%"
              }
            }
          });

          var result = [];

          for (p in products) {
            var hascategory = true;

            if (req.query.categoryid != null) {
              hascategory = products[p].categoryId == req.query.categoryid;
            }

            console.log(
              "%%%%% category id:" +
                products[p].categoryId +
                " hascat: " +
                hascategory
            );

            if (products[p].id != null && hascategory) {
              console.log(products[p].id);
              var images = await Image.findAll({
                where: { productId: products[p].id }
              });

              var cat = await Category.findOne({
                where: { id: products[p].categoryId }
              });

              result.push({
                product: products[p],
                images: images,
                category: cat
              });
            }
          }

          return res.response(result);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query product info",
      notes: "Query for product info",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query"),
          categoryid: Joi.string().description("category"),
          establishmentid: Joi.string().description("establishment")
        }
      }
    }
  },
  {
    path: "/product/category",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var establish = await Establishment.findOne({
            where: { id: req.payload.establishmentid }
          });

          console.log("establish:" + JSON.stringify(establish));

          var category = await Category.create(req.payload.category);

          category.setEstablishment(establish);
          category.save();

          var cats = await Category.findAll({
            where: { establishmentId: req.payload.establishmentid }
          });

          return res.response(cats);
        } catch (e) {
          console.log(e);
          return res.response(e);
        }
      },
      description: "Register Category",
      notes: "Register Category",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          establishmentid: Joi.string().description("establishment"),
          category: {
            id: Joi.string().description("id"),
            name: Joi.string().description("name")
          }
        }
      }
    }
  },
  {
    path: "/product/categories",
    method: "GET",
    config: {
      handler: async function(req, res) {
        console.log(req.query);

        try {
          var cats = await Category.findAll({
            where: { establishmentId: req.query.establishmentid }
          });

          return res.response(cats);
        } catch (e) {
          console.log(e);
          return res.response(e);
        }
      },
      description: "Register Category",
      notes: "Register Category",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          establishmentid: Joi.string().description("establishment")
        }
      }
    }
  }
];
