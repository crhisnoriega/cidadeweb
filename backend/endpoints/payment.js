const fs = require("fs");

const Joi = require("joi");
const { Order, User, CreditCard, Payment } = require("../persistence/db");
const { doPurchase, cancelPurchase } = require("../payment/cielopayment");
const { sendFCMMessage } = require("../firebase/fcm");

// const redis = require("../aws/elasticache");

checkOrderPayment = order => {
  var users = order.getUsers();
  var payments = order.getPayments();

  var allPay = true;

  for (user in users) {
    var userWasPay = false;

    for (payment in payments) {
      var u = payment.getUser();
      if (u.id == user.id) {
        userWasPay = true;
        break;
      }
    }

    if (!userWasPay) {
      allPay = false;
    }
  }

  return allPay;
};

module.exports = [
  {
    path: "/payment/do",
    method: "POST",
    config: {
      handler: async function(req, res) {
        try {
          // var orderJson = await redis.get(req.payload.orderid);

          var order = await Order.findOne({
            where: { id: req.payload.orderid }
          });

          if (order == null) {
            return res.response({ error: "order dont found" });
          }

          var userOwner = await User.findOne({
            where: { id: order.ownerUser }
          });

          var creditcard = await CreditCard.findOne({
            where: { id: req.payload.creditcardid }
          });

          if (creditcard == null) {
            return res.response({ error: "creditcard dont found" });
          }

          var payment = await Payment.create({
            totalAmount: order.totalPrice,
            status: "initialize"
          });

          var paymentResult = await doPurchase(
            order.id,
            creditcard,
            order.totalPrice,
            payment
          );

          order.addPayment(paymentResult);

          if ((paymentResult.status = "authorized")) {
            if (paymentResult.type == "shared") {
              if (checkOrderWasPay(order)) {
                sendFCMMessage(userOwner.fcmToken, { confirmedOrder: order });
              }
            }
          } else {
            if (paymentResult.type == "shared") {
              sendFCMMessage(userOwner.fcmToken, { notAuthorizedOrder: order });
            }
          }

          return res.response({ status: paymentResult });
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Make purchases",
      notes: "Make purchases",
      tags: ["api", "payment"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          userid: Joi.string().required(),
          orderid: Joi.string().required(),
          creditcardid: Joi.string().required()
        }
      }
    }
  },
  {
    path: "/payment/cancel/{paymentid}",
    method: "POST",
    config: {
      handler: async function(req, res) {
        try {
          var order = await Order.findOne({
            where: { id: req.payload.orderid }
          });

          if (order == null) {
            return res.response({ error: "order dont found" });
          }

          var creditcard = await CreditCard.findOne({
            where: { id: req.payload.creditcardid }
          });

          if (creditcard == null) {
            return res.response({ error: "creditcard dont found" });
          }

          var payment = await Payment.create({
            totalAmount: order.totalPrice,
            status: "initialize"
          });

          var result = await doPurchase(
            order.id,
            creditcard,
            order.totalPrice,
            payment
          );

          order.addPayment(payment);

          return res.response({ status: result });
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Cancel purchases",
      notes: "Cancel purchases",
      tags: ["api", "payment"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          orderid: Joi.string().required(),
          creditcardid: Joi.string().required()
        }
      }
    }
  },
  {
    path: "/payment/addcard",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          req.payload.cardType = "default";

          var creditcard = await CreditCard.create(req.payload);

          return res.response({ status: "OK", creditcardid: creditcard.id });
        } catch (e) {
          console.log(e);
          return res.response({ error: e });
        }
      },
      description: "Register CreditCard",
      notes: "Register CreditCard",
      tags: ["api", "payment"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          cardNumber: Joi.string().required(),
          holder: Joi.string().required(),
          expirationDate: Joi.string().required(),
          securityCode: Joi.string().required(),
          brand: Joi.string().required()
        }
      }
    }
  }
];
