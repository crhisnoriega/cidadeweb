const Joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const { User } = require("../persistence/db");

module.exports = [
  {
    method: "GET",
    path: "/user/auth",
    config: {
      handler: async function (req, res) {
        var user = await User.findOne({ where: { login: req.query.login } });

        if (user == null) {
          return res.response({ error: "user not found" }).code(201);
        }

        /*var comparePassword = await bcrypt.compare(
          user.password,
          req.query.password
        );*/

        if (user.password != req.query.password) {
          return res.response({ error: "wrong password" }).code(201);
        }

        var token = jwt.sign({
            data: user.login
          },
          "kiwkiwrocks", { expiresIn: 60 * 60 * 60 }
        );
        user.token = token;
        user.tokenDate = new Date().toISOString();
        user.fcmToken = "";
        user.save();

        user.password = null;
        return res.response(user).code(201);
      },
      description: "Authenticate user",
      notes: "Return session token",
      tags: ["api", "user"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          login: Joi.string()
            .required()
            .description("username"),
          password: Joi.string()
            .required()
            .description("password"),
          fcmToken: Joi.string()
            .required()
            .description("FCM token")
        }
      }
    }
  },
  {
    path: "/user/{userid}",
    method: "PUT",
    config: {
      handler: async function (req, res) {
        var result = await User.update(req.payload, {
          where: { id: req.params.userid }
        });

        if (result[0] == 1) {
          return res.response({ status: "OK" });
        } else {
          return res.response({ status: "error" });
        }
      },
      description: "Update user info",
      tags: ["api", "user"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          userid: Joi.string()
            .required()
            .description("user id")
        },
        payload: {
          firstName: Joi.string().description("user's firt name"),
          lastName: Joi.string().description("user's last name"),
          email: Joi.string().description("user's email"),
          password: Joi.string().description("user's password")
        }
      }
    }
  },
  {
    path: "/user",
    method: "POST",

    config: {
      handler: async function (req, res) {
        console.log(req.payload);


        // req.payload.password = bcrypt.hashSync(req.payload.password, 1);
        req.payload.password = req.payload.password;
        req.payload.status = "new";

        var user = await User.findOne({ where: { login: req.payload.login } });
        if (user != null) {
          return res.response({ status: "user already register" }).code(501);
        }

        try {
          var user = await User.create(req.payload);
          return res.response(user);
        } catch (e) {
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register new user",
      tags: ["api", "user"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          login: Joi.string()
            .required()
            .description("username"),
          phone: Joi.string()
            .required()
            .description("main phone user"),
          firstName: Joi.string()
            .required()
            .description("user's firt name"),
          lastName: Joi.string()
            .required()
            .description("user's last name"),
          email: Joi.string()
            .required()
            .description("user's email"),
          password: Joi.string()
            .required()
            .description("user's password")
        }
      }
    }
  }
];
