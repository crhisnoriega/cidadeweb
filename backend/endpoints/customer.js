const fs = require("fs");

const Joi = require("joi");
const { Counter, Customer, Property, Image } = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/customer",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        if (req.payload.id == null) {
          req.payload.id = uuidv1();
        }

        console.log(req.payload);

        try {
          var customer = await Customer.create(req.payload);
          customer.save();

          return res.response(customer).code(200);
        } catch (e) {
          console.log("error");
          console.log(JSON.stringify(e));
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register Customer",
      notes: "Register Customer",
      tags: ["api", "customer"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/customer/{customerid}",
    method: "PUT",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var customer = await Customer.update(req.payload, {
            where: { id: req.params.customerid }
          });

          return res.response(customer);
        } catch (e) {
          console.log("error");
          console.log(JSON.stringify(e));
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register Customer",
      notes: "Register Customer",
      tags: ["api", "customer"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          customerid: Joi.string()
            .required()
            .description("base64 endode")
        },
        payload: {}
      }
    }
  },
  {
    path: "/customer/{customerid}",
    method: "GET",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var customer = await Customer.findOne({
            where: { id: req.params.customerid }
          });

          return res.response(customer);
        } catch (e) {
          console.log("error");
          console.log(JSON.stringify(e));
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register Customer",
      notes: "Register Customer",
      tags: ["api", "customer"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          customerid: Joi.string()
            .required()
            .description("base64 endode")
        }
      }
    }
  },
  {
    path: "/customer/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var customers = await Customer.findAll({
            where: {
              [Op.or]: [
                {
                  name: {
                    [Op.like]: "%" + req.query.query + "%"
                  }
                },
                {
                  codigo: {
                    [Op.like]: "%" + req.query.query + "%"
                  }
                }
              ]
            },
            order: [["createdAt", "DESC"]]
          });

          return res.response(customers);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query Customer info",
      notes: "Query for Customer info",
      tags: ["api", "customer"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query")
        }
      }
    }
  },

  {
    path: "/customer/counter",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var counter = await Counter.findOne({
            where: { id: "customer_counter" }
          });

          if (counter == null || counter == []) {
            counter = new Counter();
            counter.id = "customer_counter";
            Counter.create(counter);
          }

          counter.counter = counter.counter + 1;

          counter.save();

          return res.response(counter);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query sub-category info",
      notes: "Query for sub-category info",
      tags: ["api", "property"],
      validate: {
        options: {
          allowUnknown: true
        }
      }
    }
  }
];
