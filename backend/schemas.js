const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our data base's data structure 
const DataSchema = new Schema({
    id: Number,
    message: String,
    myid: [String]
}, { timestamps: true });

const ImageContentSchema = new Schema({
    owner: String,
    name: String,
    content: String,
}, {timestamps: true });

const UserSchema = new Schema({
    login: String,
    name: String,
    email: String,
    password: String,
    account_id: String,
    token: String,
    spirit: Object,
    info: Object,
    token_firebase: String
}, { timestamps: true });

const UserInfoSchema = new Schema({
    born: Date,
    bio: String,
    address: String,
}, {timestamps: true});

const UserSpiritSchema = new Schema({
    account_id: String,
    biography: String,
    personality: String,
    horoscopo: String,
    generous: String,
    romance: String,
    focus_activity: [String],
    music_style: [String],
    interests: [String],

    birth: Date,
    fone1: String,
    fone2: String,
    genre: String,
    
    verified: Boolean,
    account_type: String,
    account_complete: Boolean
}, { timestamps: true });

const FavsUserSchema = new Schema({
    login: String,
    fav_login: String,
    status: Number
}, { timestamps: true });

const AccountFeatureSchema = new Schema({
    feature_id: String,
    description: String,
    value: String,
}, { timestamps: true });

const RelationUserProjectSchema = new Schema({
    project_id: String,
    login: String
});

// export the new Schema so we could modify it using Node.js
const Data = mongoose.model("Data", DataSchema);
const User = mongoose.model("User", UserSchema);
const RelationUserProject = mongoose.model("RelationUserProject", RelationUserProjectSchema);
const UserSpirit = mongoose.model("UserSpirit", UserSpiritSchema);
const AccountFeature = mongoose.model("AccountFeature", AccountFeatureSchema);
const FavsUser = mongoose.model("FavsUser", FavsUserSchema);
const UserInfo = mongoose.model("UserInfo", UserInfoSchema);
const ImageContent = mongoose.model("ImageContent", ImageContentSchema);

module.exports = { ImageContent, User, Data, RelationUserProject, UserInfo, UserSpirit, AccountFeature, FavsUser };
