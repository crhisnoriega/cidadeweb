module.exports = function (Sequelize, sequelize) {
  class Property extends Sequelize.Model {}
  Property.init({
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    codigo: Sequelize.STRING,
    name: Sequelize.STRING,

    categories: {
      type: Sequelize.STRING,
      get: function () {
        try {
          var json = JSON.parse(this.getDataValue("categories"));
          console.log(json);
          return json;
        } catch (e) {
          console.log(e);
        }
      },
      set: function (val) {
        try {
          return this.setDataValue("categories", JSON.stringify(val));
        } catch (e) {}
      }
    },

    classification: {
      type: Sequelize.STRING,
      get: function () {
        try {
          return JSON.parse(this.getDataValue("classification"));
        } catch (e) {}
      },
      set: function (val) {
        try {
          return this.setDataValue("classification", JSON.stringify(val));
        } catch (e) {}
      }
    },

    resources: {
      type: Sequelize.TEXT('long'),
      get: function () {
        try {
          return JSON.parse(this.getDataValue("resources"));
        } catch (e) {}
      },
      set: function (val) {
        try {
          return this.setDataValue("resources", JSON.stringify(val));
        } catch (e) {}
      }
    },

    destiny: {
      type: Sequelize.TEXT('long'),
      get: function () {
        try {
          return JSON.parse(this.getDataValue("destiny"));
        } catch (e) {}
      },
      set: function (val) {
        try {
          return this.setDataValue("destiny", JSON.stringify(val));
        } catch (e) {}
      }
    },

    subcategory: {
      type: Sequelize.TEXT('long'),
      get: function () {
        try {
          return JSON.parse(this.getDataValue("subcategory"));
        } catch (e) {}
      },
      set: function (val) {
        try {
          return this.setDataValue("subcategory", JSON.stringify(val));
        } catch (e) {}
      }
    },

    status: {
      type: Sequelize.ENUM,
      values: ["enable", "disable", "unauthorized", "recused", "initialize"]
    },

    capacity: Sequelize.DOUBLE,
    distance: Sequelize.DOUBLE,

    shortName: Sequelize.STRING,
    description: Sequelize.STRING,

    beach: Sequelize.STRING,

    price: Sequelize.STRING,

    cep: Sequelize.STRING,
    street: Sequelize.STRING,
    number: Sequelize.STRING,
    complemento: Sequelize.STRING,
    bairro: Sequelize.STRING,
    city: Sequelize.STRING,

    state: {
      type: Sequelize.STRING,
      get: function () {
        try {
          return JSON.parse(this.getDataValue("state"));
        } catch (e) {}
      },
      set: function (val) {
        try {
          return this.setDataValue("state", JSON.stringify(val));
        } catch (e) {}
      }
    },
    fone1: Sequelize.STRING,
    fone2: Sequelize.STRING,
    email: Sequelize.STRING,

    rooms: Sequelize.DOUBLE,

    latitude: Sequelize.DOUBLE,
    longitude: Sequelize.DOUBLE,

    img: Sequelize.TEXT('long')
  }, { sequelize, modelName: "property" });

  return Property;
};
