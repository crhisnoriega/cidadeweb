module.exports = function(Sequelize, sequelize) {
  class Customer extends Sequelize.Model {}
  Customer.init({
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    codigo: Sequelize.STRING,
    name: Sequelize.STRING,
    plano: {
      type: Sequelize.TEXT("long"),
      get: function() {
        try {
          var json = JSON.parse(this.getDataValue("plano"));
          console.log(json);
          return json;
        }
        catch (e) {
          console.log(e);
        }
      },
      set: function(val) {
        try {
          return this.setDataValue("plano", JSON.stringify(val));
        }
        catch (e) {}
      }
    },
    cpf: Sequelize.STRING,
    rg: Sequelize.STRING,

    nacio: Sequelize.STRING,
    civil: {
      type: Sequelize.TEXT("long"),
      get: function() {
        try {
          var json = JSON.parse(this.getDataValue("civil"));
          console.log(json);
          return json;
        }
        catch (e) {
          console.log(e);
        }
      },
      set: function(val) {
        try {
          return this.setDataValue("civil", JSON.stringify(val));
        }
        catch (e) {}
      }
    },
    obs: Sequelize.STRING,

    status: {
      type: Sequelize.ENUM,
      values: ["enable", "disable", "unauthorized", "recused", "initialize"]
    },

    capacity: Sequelize.DOUBLE,
    distance: Sequelize.DOUBLE,

    shortName: Sequelize.STRING,
    description: Sequelize.STRING,

    beach: Sequelize.STRING,

    cep: Sequelize.STRING,
    street: Sequelize.STRING,
    number: Sequelize.STRING,
    complemento: Sequelize.STRING,
    bairro: Sequelize.STRING,
    city: Sequelize.STRING,
    state: Sequelize.STRING,
    fone1: Sequelize.STRING,
    fone2: Sequelize.STRING,
    email: Sequelize.STRING,

    rooms: Sequelize.DOUBLE,

    latitude: Sequelize.DOUBLE,
    longitude: Sequelize.DOUBLE,

    img: Sequelize.TEXT("long"),

    dependency: {
      type: Sequelize.TEXT("long"),
      get: function() {
        try {
          var json = JSON.parse(this.getDataValue("dependency"));
          console.log(json);
          return json;
        }
        catch (e) {
          console.log(e);
        }
      },
      set: function(val) {
        try {
          return this.setDataValue("dependency", JSON.stringify(val));
        }
        catch (e) {}
      }
    },
  }, { sequelize, modelName: "customer" });

  return Customer;
};
