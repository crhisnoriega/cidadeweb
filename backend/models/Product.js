module.exports = function(Sequelize, sequelize, Establishment, Category) {
  class Product extends Sequelize.Model {}
  Product.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      name: Sequelize.STRING,
      unit: Sequelize.STRING,
      unitPrice: Sequelize.DOUBLE,
      imageUrl: Sequelize.STRING,
      version: Sequelize.INTEGER,      
    },
    { sequelize, modelName: "product" }
  );


  Product.belongsTo(Establishment);
  Product.belongsTo(Category);

  return Product;
};
