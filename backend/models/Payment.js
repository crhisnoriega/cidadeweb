module.exports = function(Sequelize, sequelize, User) {
  class Payment extends Sequelize.Model {}
  Payment.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      totalAmount: Sequelize.DOUBLE,
      value: Sequelize.DOUBLE,
      creditCardRef: Sequelize.STRING,
      cieloPaymentId: Sequelize.STRING,
      cieloTransaction: Sequelize.STRING(1024),
      status: {
        type: Sequelize.ENUM,
        values: [
          "canceled",
          "authorized",
          "unauthorized",
          "recused",
          "initialize"
        ]
      }
    },
    { sequelize, modelName: "payment" }
  );

  Payment.belongsTo(User);

  return Payment;
};
