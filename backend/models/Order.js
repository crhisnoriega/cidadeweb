module.exports = function(Sequelize, sequelize, User, Waitress, Payment) {
  class Order extends Sequelize.Model {}
  Order.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      ownerUser: Sequelize.STRING,
      totalAmount: Sequelize.DOUBLE,
      type: {
        type: Sequelize.ENUM,
        values: ["individual", "shared"]
      },
      status: {
        type: Sequelize.ENUM,
        values: ["pending", "new", "pay", "recused", "waiting_aproval"]
      },
      dateStatus: Sequelize.DATE
    },
    { sequelize, modelName: "order" }
  );

  class OrderItem extends Sequelize.Model {}
  OrderItem.init(
    {
      productId: Sequelize.STRING,

      name: Sequelize.STRING,
      unit: Sequelize.STRING,
      unitPrice: Sequelize.DOUBLE,
      quantity: Sequelize.DOUBLE,
      totalAmount: Sequelize.DOUBLE
    },
    { sequelize, modelName: "orderitem" }
  );

  Order.belongsToMany(User, { through: "order_user" });
  Order.belongsToMany(Waitress, { through: "order_waitress" });
  Order.belongsToMany(Payment, { through: "order_payment" });

  OrderItem.belongsTo(Order);
  Order.hasMany(OrderItem);

  return { Order, OrderItem };
};
