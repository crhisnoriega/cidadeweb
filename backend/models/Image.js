
module.exports = function(Sequelize, sequelize) {
  class Image extends Sequelize.Model {}
  Image.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      propertyid: Sequelize.STRING,
      url: Sequelize.STRING,
      base64: Sequelize.TEXT('long')
    },
    { sequelize, modelName: "image" }
  );

  return Image;
};
