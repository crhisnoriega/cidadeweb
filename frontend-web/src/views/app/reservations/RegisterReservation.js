import React, { Component } from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import {
  FormikReactSelect,
  FormikCheckboxGroup,
  FormikCheckbox,
  FormikRadioButtonGroup,
  FormikCustomCheckbox,
  FormikCustomCheckboxGroup,
  FormikCustomRadioGroup,
  FormikTagsInput,
  FormikSwitch,
  FormikDatePicker
}
from "../FormikFields";
import {
  Row,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Button,
  Input,
  FormFeedback,
  CustomInput
}
from "reactstrap";
import { Colxx, Separator } from "../../../components/common/CustomBootstrap";
import ReactSiemaCarousel from "../../../components/ReactSiema/ReactSiemaCarousel";
import { NotificationManager } from "../../../components/common/react-notifications";


import IntlMessages from "../../../helpers/IntlMessages";

import Breadcrumb from "../../../containers/navs/Breadcrumb";

import InputMask from "react-input-mask";
import request from "sync-request";
import axios from "axios";

import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import ThemedStyleSheet from "react-with-styles/lib/ThemedStyleSheet";
import DefaultTheme from "react-dates/lib/theme/DefaultTheme";
import moment from "moment";
import "moment/locale/pt-br";

import Select from "react-select";
import AsyncSelect from "react-select/async";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

const SignupSchema = Yup.object().shape({
  customer: Yup.object()
    .typeError("Cliente da reserva")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),

  property: Yup.object()
    .typeError("Imovel da reserva")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),

  status: Yup.object()
    .typeError("Selecione o status inicial")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),

  capacity: Yup.string().required("Capacidade da reserva"),

  reservation_date: Yup.object().typeError("Data da reserva").required("Data da reserva")
});



const status_list = [
  { value: "enable", label: "Confirmada" },
  { value: "disable", label: "Cancelada" },
  { value: "initialize", label: "Em Andamento" },
  { value: "recused", label: "Em Avaliação" }
];

const _init = {
  customer: null,
  property: null,
  capacity: 1,
  reservation_date: null,
  status: null
};

class FormikCustomComponents extends Component {
  constructor(props) {
    super(props);

    this.state = {
      init: _init,
      images: [],
      startDate: null,
      endDate: null
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    var reservationid = localStorage.getItem("reservation_id");


    if (reservationid) {
      axios
        .get("/reservation/" + reservationid, {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          localStorage.removeItem("reservation_id")

          console.log(res.data)
          console.log(Date.parse(res.data.start_date))

          var reservation = res.data.reservation;
          var customer = res.data.customer;
          var property = res.data.property;
          var statusss = null

          status_list.forEach(item => {
            if (item.value == reservation.status) statusss = item;
          })

          this.setState({
            codigo: reservation.codigo,
            startDate: moment(reservation.start_date),
            endDate: moment(reservation.end_date),
            init: {
              customer: customer,
              property: property,
              reservation_date: moment(reservation.createdAt, 'YYYY-MM-DD'),
              capacity: reservation.capacity,
              description: reservation.description,
              status: statusss
            }
          })
        })
        .catch(error => {
          localStorage.removeItem("reservation_id")
          console.log(error);
        });
    } else {
      axios
        .get("/properties/counter", {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          this.setState({ codigo: "00000" + res.data.counter + "-001" });
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  handleSubmit = (values, { setSubmitting }) => {

    if (this.state.startDate == null || this.state.endDate == null) {
      NotificationManager.error(
        "Nenhum periodo selecionado",
        "Cancelar",
        5000,
        () => {},
        null,
        "cName"
      );
      setSubmitting(false);
      return;
    }

    values.codigo = this.state.codigo;
    values.start_date = this.state.startDate;
    values.end_date = this.state.endDate;

    values.customer_code = values.customer.value;
    values.property_code = values.property.value;

    console.log(values);

    axios
      .post("/reservation", values, {
        headers: {
          "content-type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({ init: _init })
        this.props.history.push("/app/reservation/calendar");
        setSubmitting(false);
      })
      .catch(error => {
        console.log(error);
        setSubmitting(false);
      });
  };

  removeImage = image => {
    let filteredArray = this.state.images.filter(
      item => item.name !== image.name
    );
    this.setState({ images: filteredArray, filename: "" });
  };

  handleFileSelect = e => {
    console.log(e.target.files[0].name);
    let file = e.target.files[0];

    // Make new FileReader
    let reader = new FileReader();

    // Convert the file to base64 text
    reader.readAsDataURL(file);

    // on reader load somthing...
    reader.onload = () => {
      // Make a fileInfo Object
      let fileInfo = {
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + " kB",
        base64: reader.result,
        file: file
      };

      this.setState({
        images: [fileInfo, ...this.state.images]
      });
    };
  };


  searchCustomer = inputValue => {
    if (inputValue.length < 0) return null;

    return axios
      .get("/customer/search?query=" + inputValue, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        var results = [];
        res.data.forEach(customer => {
          results.push({
            value: customer.id,
            label: customer.codigo + " - " + customer.name
          });
        });
        return results;
      })
      .catch(error => {
        console.log(error);
        return [];
      });
  };

  searchProperty = inputValue => {
    return axios
      .get("/properties/search?query=" + inputValue, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);
        var results = [];
        res.data.forEach(property => {
          results.push({
            value: property.id,
            label: property.codigo + " - " + property.name
          });
        });

        return results;
      })
      .catch(error => {
        console.log(error);
        return [];
      });
  };

  render() {
    console.log("state: " + this.state.images);
    console.log("init: " + JSON.stringify(this.state.init));
    return (
      <Row className="mb-4">
        <Row>
          <Colxx xxs="12">
            <Breadcrumb
              heading="Cadastro de Reserva"
              match={this.props.match}
            />
            <Separator className="mb-5" />
          </Colxx>
        </Row>
        <Colxx xxs="12">
          <Card>
            <CardBody>
              <Formik
                enableReinitialize
                initialValues={this.state.init}
                validationSchema={SignupSchema}
                onSubmit={this.handleSubmit}
              >
                {({
                  handleSubmit,
                  setFieldValue,
                  setFieldTouched,
                  handleChange,
                  handleBlur,
                  values,
                  errors,
                  touched,
                  isSubmitting
                }) => (
                  <Form className="av-tooltip tooltip-label-right">
                    <FormGroup className="col-md-4">
                      <Label className="green-label" for="codigo">
                        Codigo de Reserva
                      </Label>
                      <Input
                        disabled
                        invalid={touched.codigo && !!errors.codigo}
                        type="text"
                        name="codigo"
                        id="codigo"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={this.state.codigo}
                      />
                      <FormFeedback>{errors.codigo}</FormFeedback>
                    </FormGroup>

                    <FormGroup className="col-md-8 error-l-100">
                      <Label className="green-label" for="customer">
                        Cliente
                      </Label>
                      <AsyncSelect
                        name="customer"
                        className={`react-select ${this.props.className}`}
                        classNamePrefix="react-select"
                        isMulti={false}
                        onChange={e => {
                          console.log(e);
                          setFieldValue("customer", e);
                        }}
                        onBlur={this.handleBlur}
                        value={values.customer}
                        loadOptions={this.searchCustomer}
                        placeholder={"Codigo ou nome do cliente"}
                      />
                       {errors.customer && touched.customer ? (
                            <div className="invalid-feedback d-block">
                              {JSON.stringify(errors.customer)}
                            </div>
                          ) : null}
                    </FormGroup>

                    <FormGroup className="col-md-8 error-l-100">
                      <Label className="green-label" for="name">
                        Imovel
                      </Label>
                      <AsyncSelect
                        name="property"
                        className={`react-select ${this.props.className}`}
                        classNamePrefix="react-select"
                        isMulti={false}
                        onChange={e => {
                          console.log(e);
                          setFieldValue("property", e);
                        }}
                        onBlur={this.handleBlur}
                        value={values.property}
                        loadOptions={this.searchProperty}
                        placeholder={"Codigo ou nome do imovel"}
                      />
                        {errors.property && touched.property ? (
                          <div className="invalid-feedback d-block">
                              {JSON.stringify(errors.property)}
                            </div>
                          ) : null}
                    </FormGroup>

                    <FormGroup className="col-md-4 error-l-100">
                      <Label className="green-label" for="reservation_date">
                        Data da Reserva
                      </Label>
                      <FormikDatePicker
                        name="reservation_date"
                        value={values.reservation_date}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                      />
                      {errors.reservation_date && touched.reservation_date ? (
                        <div className="invalid-feedback d-block">
                           {JSON.stringify(errors.reservation_date)}
                        </div>
                      ) : null}
                    </FormGroup>

                    <FormGroup className="col-md-8 reservation_date">
                      <Label className="green-label" for="codigo">
                        Periodo de Solicitação
                      </Label>
                      <br />
                      <DateRangePicker
                        startDatePlaceholderText="Data Inicio"
                        endDatePlaceholderText="Data Final"
                        isOutsideRange={() => false}
                        startDate={this.state.startDate}
                        startDateId="startDate"
                        endDate={this.state.endDate}
                        endDateId="endDate"
                        onDatesChange={({ startDate, endDate }) =>{
                          console.log(startDate);
                          console.log(endDate);
                          this.setState({ startDate, endDate })
                          }
                        }
                        focusedInput={this.state.focusedInput}
                        onFocusChange={focusedInput =>
                          this.setState({ focusedInput })
                        }
                        orientation={this.state.orientation}
                        openDirection={this.state.openDirection}
                      />
                      <FormFeedback>{"Periodo invalido"}</FormFeedback>
                    </FormGroup>

                    <FormGroup className="col-md-4">
                      <Label className="green-label" for="capacity">
                        Numero de Pessoas
                      </Label>
                      <Input
                        invalid={touched.capacity && !!errors.capacity}
                        type="text"
                        name="capacity"
                        id="capacity"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.capacity}
                      />
                        {errors.capacity && touched.capacity ? (
                            <div className="invalid-feedback d-block">
                              {JSON.stringify(errors.capacity)}
                            </div>
                          ) : null}      
                    </FormGroup>

                    <FormGroup className="error-l-100 col-md-4">
                      <Label>Status</Label>
                      
                      <FormikReactSelect
                        name="status"
                        id="status"
                        value={values.status}
                        isMulti={false}
                        options={status_list}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                      />
                      {errors.status && touched.status ? (
                        <div className="invalid-feedback d-block">
                          {JSON.stringify(errors.status)}
                        </div>
                      ) : null}
                    </FormGroup>

                    <FormGroup className="col-md-8">
                      <Label className="green-label" for="description">
                        Descrição
                      </Label>
                      <Input
                        invalid={touched.description && !!errors.description}
                        type="textarea"
                        name="description"
                        id="description"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.description}
                      />
                      <FormFeedback>{errors.description}</FormFeedback>
                    </FormGroup>

                    <LaddaButton
                      type="submit"
                      className="btn btn-primary btn-ladda"
                      loading={isSubmitting}
                      onClick={e => {
                        handleSubmit(e);
                      }}
                    >
                      Confirmar
                    </LaddaButton>
                  </Form>
                )}
              </Formik>
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    );
  }
}
export default FormikCustomComponents;
