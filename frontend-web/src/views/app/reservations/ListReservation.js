import React, { Component, Fragment } from "react";
import { Row } from "reactstrap";

import axios from "axios";

import { servicePath } from "../../../constants/defaultValues";

import DataListViewReservation from "./DataListViewReservation";

import Pagination from "../../../containers/pages/Pagination";
import ContextMenuContainer from "../../../containers/pages/ContextMenuContainer";
import ListPageHeading from "../../../containers/pages/ListPageHeading";
import ImageListView from "../../../containers/pages/ImageListView";
import ThumbListView from "../../../containers/pages/ThumbListView";
import AddNewModal from "../../../containers/pages/AddNewModal";

import { Colxx, Separator } from "../../../components/common/CustomBootstrap";
import IntlMessages from "../../../helpers/IntlMessages";

import Breadcrumb from "../../../containers/navs/Breadcrumb";
import ChangeReservationStatusModal from "../../../containers/applications/ChangeReservationStatusModal";

function collect(props) {
  return { data: props.data };
}
const apiUrl = servicePath + "/cakes/paging";

class DataListPages extends Component {
  constructor(props) {
    super(props);
    this.mouseTrap = require("mousetrap");

    this.state = {
      displayMode: "list",

      selectedPageSize: 10,
      orderOptions: [
        { column: "name", label: "Nome" },
        { column: "cpf", label: "CPF" }
      ],
      pageSizes: [10, 20, 30, 50, 100],

      categories: [
        { label: "Cakes", value: "Cakes", key: 0 },
        { label: "Cupcakes", value: "Cupcakes", key: 1 },
        { label: "Desserts", value: "Desserts", key: 2 }
      ],

      selectedOrderOption: { column: "name", label: "Nome" },
      dropdownSplitOpen: false,
      modalOpen: false,
      currentPage: 1,
      totalItemCount: 0,
      totalPage: 1,
      search: "",
      selectedItems: [],
      lastChecked: null,
      isLoading: false,

      showStatus: false,

      selected: null
    };
  }
  componentDidMount() {
    this.dataListRender();
    this.mouseTrap.bind(["ctrl+a", "command+a"], () =>
      this.handleChangeSelectAll(false)
    );
    this.mouseTrap.bind(["ctrl+d", "command+d"], () => {
      this.setState({
        selectedItems: []
      });
      return false;
    });
  }

  componentWillUnmount() {
    this.mouseTrap.unbind("ctrl+a");
    this.mouseTrap.unbind("command+a");
    this.mouseTrap.unbind("ctrl+d");
    this.mouseTrap.unbind("command+d");
  }

  toggleModal = () => {
    this.setState({
      modalOpen: !this.state.modalOpen
    });
  };

  changeOrderBy = column => {
    this.setState({
        selectedOrderOption: this.state.orderOptions.find(
          x => x.column === column
        )
      },
      () => this.dataListRender()
    );
  };
  changePageSize = size => {
    this.setState({
        selectedPageSize: size,
        currentPage: 1
      },
      () => this.dataListRender()
    );
  };
  changeDisplayMode = mode => {
    this.setState({
      displayMode: mode
    });
    return false;
  };

  onChangePage = page => {
    this.setState({
        currentPage: page
      },
      () => this.dataListRender()
    );
  };

  onSearchKey = e => {
    if (e.key === "Enter") {
      this.setState({
          search: e.target.value.toLowerCase()
        },
        () => this.dataListRender()
      );
    }
  };

  onCheckItem = (event, id) => {
    localStorage.setItem("reservation_id", id);

    if (
      event.target.tagName === "A" ||
      (event.target.parentElement && event.target.parentElement.tagName === "A")
    ) {
      return true;
    }
    if (this.state.lastChecked === null) {
      this.setState({
        lastChecked: id
      });
    }

    let selectedItems = this.state.selectedItems;
    if (selectedItems.includes(id)) {
      selectedItems = selectedItems.filter(x => x !== id);
    } else {
      selectedItems.push(id);
    }
    this.setState({
      selectedItems
    });

    if (event.shiftKey) {
      var items = this.state.items;
      var start = this.getIndex(id, items, "id");
      var end = this.getIndex(this.state.lastChecked, items, "id");
      items = items.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...items.map(item => {
          return item.id;
        })
      );
      selectedItems = Array.from(new Set(selectedItems));
      this.setState({
        selectedItems
      });
    }
    document.activeElement.blur();
  };

  getIndex(value, arr, prop) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i][prop] === value) {
        return i;
      }
    }
    return -1;
  }
  handleChangeSelectAll = isToggle => {
    if (this.state.selectedItems.length >= this.state.items.length) {
      if (isToggle) {
        this.setState({
          selectedItems: []
        });
      }
    } else {
      this.setState({
        selectedItems: this.state.items.map(x => x.id)
      });
    }
    document.activeElement.blur();
    return false;
  };

  dataListRender() {
    const {
      selectedPageSize,
      currentPage,
      selectedOrderOption,
      search
    } = this.state;

    axios
      .get("/reservation/search/complete?query=%%", {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        this.setState({
          totalPage: 1,
          items: res.data,
          selectedItems: [],
          totalItemCount: res.data.length,
          isLoading: true
        });
      });


  }

  onContextMenuClick = (e, data, target) => {
    console.log(
      "onContextMenuClick - selected items",
      this.state.selectedItems
    );
    console.log("onContextMenuClick - action : ", data.action);
  };

  onContextMenu = (e, data) => {
    const clickedProductId = data.data;
    if (!this.state.selectedItems.includes(clickedProductId)) {
      this.setState({
        selectedItems: [clickedProductId]
      });
    }

    return true;
  };

  toggleModal = () => {
    this.setState({
      showStatus: !this.state.showStatus
    });
  };

  handleChangeStatus = (status, description) => {
    console.log(this.state.selected);
    console.log(status);
    console.log(description);

    axios
      .put("/reservation/status/" + this.state.selected.reservation.id, { status: status }, {
        headers: {
          "content-type": "application/json"
        }
      })
      .then(res => {
        console.log(res.data);

        this.dataListRender();

      })
      .catch(error => {
        console.log(error);

      });

  }

  render() {
    const {
      currentPage,
      items,
      displayMode,
      selectedPageSize,
      totalItemCount,
      selectedOrderOption,
      selectedItems,
      orderOptions,
      pageSizes,
      modalOpen,
      categories
    } = this.state;
    const { match } = this.props;
    const startIndex = (currentPage - 1) * selectedPageSize;
    const endIndex = currentPage * selectedPageSize;

    return !this.state.isLoading ? (
      <div className="loading" />
    ) : (
      <Fragment>
        <div className="disable-text-selection">
         <ChangeReservationStatusModal
          callback={this.handleChangeStatus}
          toggleModal={this.toggleModal}
          modalOpen={this.state.showStatus}
          surveyListApp={{
            labels: [{ value: "filho", label: "Filho" }],
            categories: ["Filho", "Mae", "Avo", "Avó"]
          }}
        />
          <Row>
            <Colxx xxs="12">
              <Breadcrumb
                heading="Lista de Reservas"
                match={this.props.match}
              />
              <Separator className="mb-5" />
            </Colxx>
          </Row>
          <ListPageHeading
            heading="menu.data-list"
            displayMode={displayMode}
            changeDisplayMode={this.changeDisplayMode}
            handleChangeSelectAll={this.handleChangeSelectAll}
            changeOrderBy={this.changeOrderBy}
            changePageSize={this.changePageSize}
            selectedPageSize={selectedPageSize}
            totalItemCount={totalItemCount}
            selectedOrderOption={selectedOrderOption}
            match={match}
            startIndex={startIndex}
            endIndex={endIndex}
            selectedItemsLength={selectedItems ? selectedItems.length : 0}
            itemsLength={items ? items.length : 0}
            onSearchKey={this.onSearchKey}
            orderOptions={orderOptions}
            pageSizes={pageSizes}
            toggleModal={this.toggleModal}
          />

          <Row>
            
            <div className="pl-2 d-flex flex-grow-1 min-width-zero">
              <div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
                  <p className="ml-4 mb-1 text-muted text-small truncate" style={{width:"200px"}}>{"Imovel"}</p>
                  <p className="mb-1 text-muted text-small truncate">{"Cliente"}</p>
                  <p className="mb-1 text-muted text-small w-8 w-sm-100">{"Status"}</p>
                <p className="mb-1 text-muted text-small w-10 w-sm-200">{"Periodo da reserva"}</p>
                <p className="mb-1 text-muted text-small w-20 w-sm-100"></p>
                 
              </div>
              <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
              </div>
            </div>
            
            
            
            
            
            {this.state.items.map(reservation => {
              console.log(reservation);

              return (
                <DataListViewReservation
                  key={reservation.id}
                  reservation={reservation.reservation}
                  customer={reservation.customer}
                  property={reservation.property}
                  isSelect={this.state.selectedItems.includes(reservation.id)}
                  onCheckItem={this.onCheckItem}
                  collect={collect}
                  onChangeStatus={e => {this.setState({showStatus:true, selected: reservation})}}
                />
              );
            })}{" "}
            <Pagination
              currentPage={this.state.currentPage}
              totalPage={this.state.totalPage}
              onChangePage={i => this.onChangePage(i)}
            />
            <ContextMenuContainer
              onContextMenuClick={this.onContextMenuClick}
              onContextMenu={this.onContextMenu}
            />
          </Row>
        </div>
      </Fragment>
    );
  }
}
export default DataListPages;
