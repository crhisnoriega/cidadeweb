import React, { Component } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import AppLayout from "../../layout/AppLayout";
import blankPage from "./blank-page";

import RegisterProperty from "./property/RegisterProperty"
import ListProperty from "./property/ListProperty"

import RegisterCustomer from "./customers/RegisterCustomer"
import ListCurstomers from "./customers/ListCustomers"

import RegisterReservation from "./reservations/RegisterReservation"
import Calendar from "./reservations/Calendar"
import ListReservation from "./reservations/ListReservation"




class App extends Component {
  render() {
    const { match } = this.props;

    return (
      <AppLayout>
        <Switch>
          <Route path={`${match.url}/property/register`} component={RegisterProperty} />
          <Route path={`${match.url}/property/list`} component={ListProperty} />
          
          <Route path={`${match.url}/customer/register`} component={RegisterCustomer} />
          <Route path={`${match.url}/customer/list`} component={ListCurstomers} />
          
          <Route path={`${match.url}/reservation/register`} component={RegisterReservation} />
          <Route path={`${match.url}/reservation/calendar`} component={Calendar} />
          <Route path={`${match.url}/reservation/list`} component={ListReservation} />
          
          <Redirect to="/error" />
        </Switch>
      </AppLayout>
    );
  }
}
const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(
  connect(
    mapStateToProps,
    {}
  )(App)
);
