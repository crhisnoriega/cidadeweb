import React, { Component } from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import {
  FormikReactSelect,
  FormikCheckboxGroup,
  FormikCheckbox,
  FormikRadioButtonGroup,
  FormikCustomCheckbox,
  FormikCustomCheckboxGroup,
  FormikCustomRadioGroup,
  FormikTagsInput,
  FormikSwitch,
  FormikDatePicker
}
from "../FormikFields";
import {
  Row,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Label,
  Button,
  Input,
  FormFeedback,
  CustomInput
}
from "reactstrap";
import { Colxx, Separator } from "../../../components/common/CustomBootstrap";
import ReactSiemaCarousel from "../../../components/ReactSiema/ReactSiemaCarousel";
import { NotificationManager } from "../../../components/common/react-notifications";

import IntlMessages from "../../../helpers/IntlMessages";

import Breadcrumb from "../../../containers/navs/Breadcrumb";

import InputMask from "react-input-mask";
import request from "sync-request";
import axios from "axios";

import IntlCurrencyInput from "react-intl-currency-input"
import CurrencyInput from 'react-currency-input';

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

const currencyConfig = {
  locale: "pt-BR",
  formats: {
    number: {
      BRL: {
        style: "currency",
        currency: "BRL",
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      },

    },
  },
};

const SignupSchema = Yup.object().shape({
  name: Yup.string().required("Nome requerido"),
  capacity: Yup.number()
    .typeError("Numero invalido")
    .min(1, "Minimo 1")
    .required("Obrigatorio"),
  distance: Yup.number()
    .typeError("Numero invalido")
    .min(1, "Minimo 1")
    .required("Obrigatorio"),
  categories: Yup.object()
    .typeError("Escolha uma quantidade")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),
  resources: Yup.array()
    .min(3, "Escolha pelos menos 3 recursos")
    .of(Yup.object()),

  rooms: Yup.object()
    .typeError("Escolha uma quantidade")
    .shape({
      label: Yup.string().required()
    }),

  classification: Yup.object()
    .typeError("Escolha uma classificação")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),

  destiny: Yup.object()
    .typeError("Escolha um destino")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),

  subcategory: Yup.object()
    .typeError("Escolha uma subcategoria")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),

  state: Yup.object()
    .typeError("Estado")
    .shape({
      label: Yup.string().required(),
      value: Yup.string().required()
    }),



  beach: Yup.string().required("Praia requerido"),

  cep: Yup.string().required("CEP requerido"),
  street: Yup.string().required("Rua requerida"),
  number: Yup.string().required("Numero requerido"),
  city: Yup.string().required("Cidade requerido")
});

const classification_list = [
  { value: "bronze", label: "Bronze" },
  { valie: "prata", label: "Prata" },
  { value: "ouro", label: "Ouro" },
  { value: "diamante", label: "Diamante" }
];

const resources_list = [
  { valie: "arcondicionado", label: "Ar Condicionado" },
  { value: "salacomsacada", label: "Sala com sacada" },
  { value: "salasemsacada", label: "Sala sem sacada" },
  { value: "cozinha", label: "Cozinha" },
  { value: "internet", label: "Internet" },
  { value: "garagem", label: "Garagem" },
  { value: "arealazer", label: "Area de Lazer" },
  { value: "piscina", label: "Piscina" },
  { value: "churrasindividual", label: "Currasqueira individual" },
  { value: "churrascoletival", label: "Currasqueira coletiva" }
];

const categories_list = [
  { value: "apartamento", label: "Apartamento " },
  { value: "casa", label: "Casa" },
  { value: "kitnet", label: "Kitnet" },
  { value: "loft", label: "Loft" }
];

const rooms_list = [
  { value: 1, label: "1 quarto" },
  { value: 2, label: "2 quartos" },
  { value: 3, label: "3 quartos" },
  { value: 4, label: "4 quartos" }
];

const destiny_list = [
  { value: "praias", label: "Praia" },
  { value: "aguas", label: "Aguas Termais" },
  { value: "serra", label: "Serra" },
  { value: "pescaria", label: "Pescaria" },
  { value: "convenios", label: "Convenios" }
];

const estado_list = [
  { "label": "Acre", "value": "AC" },
  { "label": "Alagoas", "value": "AL" },
  { "label": "Amapá", "value": "AP" },
  { "label": "Amazonas", "value": "AM" },
  { "label": "Bahia", "value": "BA" },
  { "label": "Ceará", "value": "CE" },
  { "label": "Distrito Federal", "value": "DF" },
  { "label": "Espírito Santo", "value": "ES" },
  { "label": "Goiás", "value": "GO" },
  { "label": "Maranhão", "value": "MA" },
  { "label": "Mato Grosso", "value": "MT" },
  { "label": "Mato Grosso do Sul", "value": "MS" },
  { "label": "Minas Gerais", "value": "MG" },
  { "label": "Pará", "value": "PA" },
  { "label": "Paraíba", "value": "PB" },
  { "label": "Paraná", "value": "PR" },
  { "label": "Pernambuco", "value": "PE" },
  { "label": "Piauí", "value": "PI" },
  { "label": "Rio de Janeiro", "value": "RJ" },
  { "label": "Rio Grande do Norte", "value": "RN" },
  { "label": "Rio Grande do Sul", "value": "RS" },
  { "label": "Rondônia", "value": "RO" },
  { "label": "Roraima", "value": "RR" },
  { "label": "Santa Catarina", "value": "SC" },
  { "label": "São Paulo", "value": "SP" },
  { "label": "Sergipe", "value": "SE" },
  { "label": "Tocantins", "value": "TO" }
]

const _init = {
  codigo: "",
  name: "",
  status: "",
  capacity: 0,
  distance: 0,

  price: 0,

  shortName: "",
  description: "",

  cep: "",
  street: "",
  number: "",
  complemento: "",
  bairro: "",
  city: "",
  state: "",
  email: "",

  beach: "",

  resources: [],
  categories: { value: "apartamento", label: "Apartamento " },
  classification: { value: "bronze", label: "Bronze" },
  rooms: { value: 1, label: "1 quarto" },
  destiny: null,
  subcategory: null
};

class FormikCustomComponents extends Component {
  constructor(props) {
    super(props);

    this.state = {
      init: _init,
      images: [],
      subcategory_list: [],
      destiny_list: [],
      codigo: 0,
      pricestate: 100
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    console.log(localStorage.getItem("property_id"));

    var propertyid = localStorage.getItem("property_id");

    localStorage.removeItem("property_id");

    axios
      .get("/properties/counter", {
        headers: {
          "content-type": "application/json"
        }
      })
      .then(res => {
        this.setState({ codigo: "00000" + res.data.counter });
      })
      .catch(error => {
        console.log(error);
      });

    axios
      .get("/properties/destinations", {
        headers: {
          "content-type": "application/json"
        }
      })
      .then(res => {
        this.setState({ destiny_list: res.data });
      })
      .catch(error => {
        console.log(error);
      });

    if (propertyid != null) {
      axios
        .get("/properties/" + propertyid, {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          if (res.data.price)
            res.data.price = res.data.price.replace("R$", "").replace(",", ".")

          res.data.rooms = {
            value: res.data.rooms,
            label: res.data.rooms + " quartos"
          };
          this.setState({ init: res.data, codigo: res.data.codigo });
          this.loadSubcategories(res.data.destiny);
        })
        .catch(error => {
          console.log(error);
        });

      axios
        .get("/properties/images/" + propertyid, {
          headers: {
            "content-type": "application/json"
          }
        })
        .then(res => {
          var imgs = res.data;

          console.log(imgs);

          var imgsbase64 = [];

          for (var i = 0; i < imgs.length; i++) {
            if (imgs[i].base64) {
              imgsbase64.push({
                name: "teste",
                base64: imgs[i].base64
              });
            }
          }

          this.setState({
            images: imgsbase64
          });
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  handleSubmit = (values, { setSubmitting }) => {
    if (this.state.images.length == 0) {
      NotificationManager.error(
        "Nenhuma imagem adicionada",
        "Cancelar",
        5000,
        () => {},
        null,
        "cName"
      );
      setSubmitting(false);
      return;
    }

    values.codigo = this.state.codigo;
    values.rooms = values.rooms.value;
    console.log(values);

    axios
      .post("/properties", values, {
        headers: {
          "content-type": "application/json"
        }
      })
      .then(res => {
        console.log("response");
        console.log(res.data);
        this.state.images.forEach(fileinfo => {
          this.onImageUpload(fileinfo, res.data.id);
        });
        this.setState({ init: _init, images: [] });
        setSubmitting(false);

        this.props.history.push("/app/property/list");
      })
      .catch(error => {
        console.log(error);
        setSubmitting(false);
        this.props.history.push("/app/property/list");
      });
  };

  removeImage = image => {
    let filteredArray = this.state.images.filter(
      item => item.name !== image.name
    );
    this.setState({ images: filteredArray, filename: "" });
  };

  handleFileSelect = e => {
    console.log(e.target.files[0].name);
    let file = e.target.files[0];

    // Make new FileReader
    let reader = new FileReader();

    // Convert the file to base64 text
    reader.readAsDataURL(file);

    // on reader load somthing...
    reader.onload = () => {
      // Make a fileInfo Object
      let fileInfo = {
        name: file.name,
        type: file.type,
        size: Math.round(file.size / 1000) + " kB",
        base64: reader.result,
        file: file
      };

      this.setState({
        images: [fileInfo, ...this.state.images]
      });
    };
  };

  onImageUpload = (fileinfo, productid) => {
    fileinfo.file = null;

    axios
      .post("/properties/base64image/" + productid, fileinfo, {
        headers: {
          "content-type": "application/json"
        }
      })
      .then(function () {
        console.log("SUCCESS!!");
      })
      .catch(error => {
        console.log(error);
      });
  };

  loadSubcategories = value => {
    axios
      .get("/properties/categories/" + value.value, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res);
        if (res.data) {
          this.setState({ subcategory_list: res.data });
        } else {
          this.setState({ subcategory_list: [] });
        }
      })
      .catch(error => {
        console.log(error);
        this.setState({ subcategory_list: [] });
      });
  };

  render() {
    console.log("state: " + this.state.images);
    console.log("init: " + JSON.stringify(this.state.init));
    return (
      <Row className="mb-4">
        <Row>
          <Colxx xxs="12">
            <Breadcrumb heading="Cadastro de Imovel" match={this.props.match} />
            <Separator className="mb-5" />
          </Colxx>
        </Row>
        <Colxx xxs="12">
          <Card>
            <CardBody>
              <Formik
                enableReinitialize
                initialValues={this.state.init}
                validationSchema={SignupSchema}
                onSubmit={this.handleSubmit}
              >
                {({
                  handleSubmit,
                  setFieldValue,
                  setFieldTouched,
                  handleChange,
                  handleBlur,
                  values,
                  errors,
                  touched,
                  isSubmitting
                }) => (
                  <Form className="av-tooltip tooltip-label-right">
                    <FormGroup>
                      <Label className="green-label" for="codigo">
                        <IntlMessages id="forms.register.property.codigo" />
                      </Label>
                      <Input
                        disabled
                        className="col-md-4"
                        invalid={touched.codigo && !!errors.codigo}
                        type="text"
                        name="codigo"
                        id="codigo"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={this.state.codigo}
                      />
                      <FormFeedback>{errors.codigo}</FormFeedback>
                    </FormGroup>

                    <FormGroup className="error-l-140">
                      <Label className="green-label error-l-300" for="name">
                        Nome do Imovel
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.name && !!errors.name}
                        type="text"
                        name="name"
                        id="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                      {errors.name && touched.name ? (
                        <div className="invalid-feedback d-block">
                          {errors.name}
                        </div>
                      ) : null}
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="description">
                        Descrição
                      </Label>
                      <Input
                        invalid={touched.description && !!errors.description}
                        type="textarea"
                        name="description"
                        id="description"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.description}
                      />
                      <FormFeedback>{errors.description}</FormFeedback>
                    </FormGroup>

                    <Row>
                      <Colxx xs="8" md="6">
                        <FormGroup className="error-l-100">
                          <Label>Tipo</Label>
                          <FormikReactSelect
                            name="categories"
                            id="categories"
                            value={values.categories}
                            isMulti={false}
                            options={categories_list}
                            onChange={setFieldValue}
                            onBlur={setFieldTouched}
                          />
                          {errors.categories && touched.categories ? (
                            <div className="invalid-feedback d-block">
                              {errors.categories}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Colxx>

                      <Colxx xs="4" md="4">
                        <FormGroup className="error-l-100">
                          <Label>Classificação</Label>
                          <FormikReactSelect
                            name="classification"
                            id="classification"
                            value={values.classification}
                            isMulti={false}
                            options={classification_list}
                            onChange={setFieldValue}
                            onBlur={setFieldTouched}
                          />
                        </FormGroup>
                      </Colxx>
                    </Row>

                    <Row>
                      <Colxx xs="8" md="6">
                        <FormGroup className="error-l-100">
                          <Label>Recursos do Imovel</Label>
                          <FormikReactSelect
                            name="resources"
                            id="resources"
                            value={values.resources}
                            isMulti={true}
                            options={resources_list}
                            onChange={setFieldValue}
                            onBlur={setFieldTouched}
                          />
                          {errors.resources && touched.resources ? (
                            <div className="invalid-feedback d-block">
                              {JSON.stringify(errors.resources)}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Colxx>

                      <Colxx xs="3" md="4">
                        <FormGroup className="error-l-100">
                          <Label>Numero de Quartos</Label>
                          <FormikReactSelect
                            name="rooms"
                            id="rooms"
                            value={values.rooms}
                            isMulti={false}
                            options={rooms_list}
                            onChange={setFieldValue}
                            onBlur={setFieldTouched}
                          />
                          {errors.rooms && touched.rooms ? (
                            <div className="invalid-feedback d-block">
                              {errors.rooms}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Colxx>
                    </Row>

                    <Row>
                      <Colxx xs="3" md="3">
                        <FormGroup className="error-l-80">
                          <Label className="green-label" for="capacity">
                            Capacidade
                          </Label>
                          <Input
                            invalid={touched.capacity && !!errors.capacity}
                            type="text"
                            name="capacity"
                            id="capacity"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.capacity}
                            mask="999"
                            maskChar=" "
                            tag={InputMask}
                          />
                          {errors.capacity && touched.capacity ? (
                            <div className="invalid-feedback d-block">
                              {errors.capacity}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Colxx>

                      <Colxx xs="3" md="3">
                        <FormGroup>
                          <Label className="green-label" for="distance">
                            Distancia da praia (Km)
                          </Label>
                          <Input
                            invalid={touched.distance && !!errors.distance}
                            type="text"
                            name="distance"
                            id="distance"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.distance}
                            mask="9999"
                            maskChar=" "
                            tag={InputMask}
                          />
                          <FormFeedback>{errors.distance}</FormFeedback>
                        </FormGroup>
                      </Colxx>

                      <Colxx xs="3" md="3">
                        <FormGroup className="error-l-80">
                          <Label className="green-label" for="price">
                            Diaria por pessoa
                          </Label>
                          
                              <CurrencyInput 
                               prefix="R$"
                               name="price"
                            id="price"
                              className="form-control"  
                              value={values.price} 
                              onChangeEvent={handleChange}/>
                              
                              
                          {errors.price && touched.price ? (
                            <div className="invalid-feedback d-block">
                              {errors.price}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Colxx>
                    </Row>

                    <FormGroup className="error-l-100">
                      <Label className="d-block">
                        Status (Indisponivel/Disponivel)
                      </Label>
                      <FormikSwitch
                        name="status"
                        className="custom-switch custom-switch-primary"
                        value={values.status}
                        onChange={setFieldValue}
                        onBlur={setFieldTouched}
                      />
                      {errors.status && touched.status ? (
                        <div className="invalid-feedback d-block">
                          {errors.status}
                        </div>
                      ) : null}
                    </FormGroup>

                    <Row>
                      <Colxx xs="4" md="4">
                        <FormGroup className="error-l-100">
                          <Label>Destino</Label>
                          <FormikReactSelect
                            name="destiny"
                            id="destiny"
                            value={values.destiny}
                            isMulti={false}
                            options={this.state.destiny_list}
                            onChange={(e, value) => {
                              console.log(value);
                              this.loadSubcategories(value);
                              setFieldValue(e, value);
                              values.subcategory = {};
                            }}
                            onBlur={setFieldTouched}
                          />
                          {errors.destiny && touched.destiny ? (
                            <div className="invalid-feedback d-block">
                              {JSON.stringify(errors.destiny)}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Colxx>

                      <Colxx xs="4" md="4">
                        <FormGroup className="error-l-100">
                          <Label>Categoria</Label>
                          <FormikReactSelect
                            name="subcategory"
                            id="subcategory"
                            value={values.subcategory}
                            isMulti={false}
                            options={this.state.subcategory_list}
                            onChange={setFieldValue}
                            onBlur={setFieldTouched}
                          />
                          {errors.subcategory && touched.subcategory ? (
                            <div className="invalid-feedback d-block">
                              {JSON.stringify(errors.destiny)}
                            </div>
                          ) : null}
                        </FormGroup>
                      </Colxx>
                    </Row>

                    <Row>
                      <Colxx xs="8" md="6">
                        <FormGroup>
                          <Label className="green-label" for="beach">
                            Praia
                          </Label>
                          <Input
                            invalid={touched.beach && !!errors.beach}
                            type="text"
                            name="beach"
                            id="beach"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.beach}
                          />
                          <FormFeedback>{errors.beach}</FormFeedback>
                        </FormGroup>
                      </Colxx>
                    </Row>

                    <Row>
                      <Colxx xs="4" md="2">
                        <FormGroup>
                          <Label className="green-label" for="cep">
                            CEP
                          </Label>
                          <Input
                            invalid={touched.cep && !!errors.cep}
                            type="text"
                            name="cep"
                            id="cep"
                            onChange={e => {
                              handleChange(e);

                              var cep = e.target.value;
                              cep = cep.replace("-", "");
                              cep = cep.replace(" ", "");

                              console.log(cep);

                              if (cep.length == 8) {
                                var res = request(
                                  "GET",
                                  "https://viacep.com.br/ws/" + cep + "/json/"
                                );

                                console.log(res);
                                var add = JSON.parse(res.body);

                                try {
                                  values.street = add.logradouro;
                                  values.bairro = add.bairro;
                                  values.city = add.localidade;
                                } catch (e) {
                                  values.street = "";
                                  values.bairro = "";
                                  values.city = "";
                                }
                              } else {
                                values.street = "";
                                values.bairro = "";
                                values.city = "";
                              }
                            }}
                            onBlur={handleBlur}
                            value={values.cep}
                            mask="99999-999"
                            maskChar=" "
                            tag={InputMask}
                          />
                          <FormFeedback>{errors.cep}</FormFeedback>
                        </FormGroup>
                      </Colxx>

                      <Colxx xs="12" md="6">
                        <FormGroup>
                          <Label className="green-label" for="street">
                            Logradouro
                          </Label>
                          <Input
                            invalid={touched.street && !!errors.street}
                            type="text"
                            name="street"
                            id="street"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.street}
                          />
                          <FormFeedback>{errors.street}</FormFeedback>
                        </FormGroup>
                      </Colxx>

                      <Colxx xs="12" md="4">
                        <FormGroup>
                          <Label className="green-label" for="number">
                            Numero
                          </Label>
                          <Input
                            invalid={touched.number && !!errors.number}
                            type="text"
                            name="number"
                            id="number"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.number}
                          />
                          <FormFeedback>{errors.number}</FormFeedback>
                        </FormGroup>
                      </Colxx>
                    </Row>

                    <Row>
                      <Colxx xs="8" md="4">
                        <FormGroup>
                          <Label className="green-label" for="bairro">
                            Bairro
                          </Label>
                          <Input
                            invalid={touched.bairro && !!errors.bairro}
                            type="text"
                            name="bairro"
                            id="bairro"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.bairro}
                          />
                          <FormFeedback>{errors.bairro}</FormFeedback>
                        </FormGroup>
                      </Colxx>
                      <Colxx xs="8" md="4">
                        <FormGroup>
                          <Label className="green-label" for="city">
                            Cidade
                          </Label>
                          <Input
                            invalid={touched.city && !!errors.city}
                            type="text"
                            name="city"
                            id="city"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.city}
                          />
                          <FormFeedback>{errors.city}</FormFeedback>
                        </FormGroup>
                      </Colxx>

                      <Colxx xs="5" md="3">
                         <FormGroup className="error-l-100">
                            <Label>Estado</Label>
                            
                            <FormikReactSelect
                              name="state"
                              id="state"
                              value={values.state}
                              isMulti={false}
                              options={estado_list}
                              onChange={setFieldValue}
                              onBlur={setFieldTouched}
                            />
                            {errors.state && touched.state ? (
                              <div className="invalid-feedback d-block">
                                {JSON.stringify(errors.state)}
                              </div>
                            ) : null}
                          </FormGroup>
                      </Colxx>
                    </Row>

                    <FormGroup>
                      <Label className="green-label" for="beach">
                        Imagens do Imovel
                      </Label>

                      <Colxx xxs="12" className="pl-0 pr-0 mb-5">
                        <ReactSiemaCarousel
                          perPage={{
                            0: 1,
                            480: 2,
                            800: 3,
                            1200: 4
                          }}
                          controls={false}
                          loop={false}
                        >
                          {this.state.images.map(item => {
                            return (
                              <div className="pr-3 pl-3">
                                <Card>
                                  <div className="position-relative">
                                    <img
                                      className="card-img-top"
                                      src={item.base64}
                                      alt={item.name}
                                    />
                                  </div>
                                  <div className="w-50">
                                    <CardBody>
                                      <h6 className="mb-4">{item.name}</h6>
                                      <footer>
                                        <Button
                                          color="danger"
                                          onClick={e => this.removeImage(item)}
                                        >
                                          Remover
                                        </Button>
                                      </footer>
                                    </CardBody>
                                  </div>
                                </Card>
                              </div>
                            );
                          })}
                        </ReactSiemaCarousel>
                      </Colxx>

                      <Colxx xs="4" md="3">
                        <CustomInput
                          type="file"
                          label={this.state.filename}
                          onChange={e => {
                            var file = e.target.files[0];
                            this.setState({
                              file: file,
                              filename: file.name
                            });
                            this.handleFileSelect(e);
                          }}
                        />
                      </Colxx>
                    </FormGroup>

                    <LaddaButton
                      type="submit"
                      className="btn btn-primary btn-ladda"
                      loading={isSubmitting}
                      onClick={e => {
                        handleSubmit(e);
                      }}
                    >
                      Confirmar
                    </LaddaButton>
                  </Form>
                )}
              </Formik>
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    );
  }
}
export default FormikCustomComponents;
