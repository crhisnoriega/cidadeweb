import React from "react";
import {
  Row,
  Card,
  CardBody,
  CardSubtitle,
  CardImg,
  CardText,
  CustomInput,
  Badge
} from "reactstrap";
import { NavLink } from "react-router-dom";
import classnames from "classnames";
import { ContextMenuTrigger } from "react-contextmenu";
import { Colxx } from "../../components/common/CustomBootstrap";

import moment from "moment";

const ImageListView = ({ product, isSelect, collect, onCheckItem }) => {
  return (
    <Colxx sm="6" lg="4" xl="3" className="mb-3" key={product.id}>
      <ContextMenuTrigger id="menu_id" data={product.id} collect={collect}>
        <Card
          onClick={event => onCheckItem(event, product.id)}
          className={classnames({
            active: isSelect
          })}
        >
          <div className="position-relative">
            <NavLink to={`/app/property/register`} className="w-40 w-sm-100">
              <CardImg top alt={product.name} src={product.img} />
            </NavLink>
            <Badge
              color={product.status == "enable" ? "primary" : "secondary"}
              pill
              className="position-absolute badge-top-left"
            >
              {product.status == "enable" ? "Disponivel" : "Indisponivel"}
            </Badge>
          </div>
          <CardBody>
            <Row>
              <Colxx xxs="10">
                <CustomInput
                  className="item-check mb-2"
                  style={{ fontSize: "24" }}
                  type="checkbox"
                  id={`check_${product.id}`}
                  checked={isSelect}
                  onChange={() => {}}
                  label={product.name}
                />
              </Colxx>
              <Colxx xxs="10">
                <h6 style={{ marginTop: "10px" }} color="primary">
                  {product.description}
                </h6>
              </Colxx>

              <Colxx xxs="10">
                <CardText className="text-muted text-small mb-0 font-weight-light">
                  Quartos: {product.rooms}
                </CardText>
                <CardText className="text-muted text-small mb-0 font-weight-light">
                  Cidade: {product.city}
                </CardText>
                <CardText className="text-muted text-small mb-0 font-weight-light">
                  Praia: {product.beach}
                </CardText>
                <CardText className="text-muted text-small mb-0 font-weight-light">
                  Registro: {moment(product.createdAt).format("LLL")}
                </CardText>
              </Colxx>
            </Row>
          </CardBody>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(ImageListView);
