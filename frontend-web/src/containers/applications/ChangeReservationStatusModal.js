import React, { Component } from "react";
import { connect } from "react-redux";
import {
  FormGroup,
  CustomInput,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Label
} from "reactstrap";
import Select from "react-select";
import CustomSelectInput from "../../components/common/CustomSelectInput";
import IntlMessages from "../../helpers/IntlMessages";

import {
  FormikReactSelect
}
from "../../views/app/FormikFields";

const status_list = [
  { value: "enable", label: "Confirmada" },
  { value: "disable", label: "Cancelada" },
  { value: "initialize", label: "Em Andamento" },
  { value: "recused", label: "Em Avaliação" }
];


class ChangeReservationStatusModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      label: {},
      labelColor: "",
      status: null,
      description: ""
    };
  }

  addChangeStatus = () => {


    this.props.callback(this.state.status, this.state.description);
    this.props.toggleModal();

    this.setState({
      title: "",
      label: {},
      category: {},
      status: null,
      description: ""
    });
  };

  render() {
    console.log("render");
    console.log(this.props.surveyListApp);
    const { labels, categories } = this.props.surveyListApp;
    const { modalOpen, toggleModal } = this.props;
    return (
      <Modal
        isOpen={modalOpen}
        toggle={toggleModal}
        wrapClassName="modal-right"
        backdrop="static"
      >
        <ModalHeader toggle={toggleModal}>Alterar status da Reserva</ModalHeader>
        <ModalBody>
         <Label className="mt-4">Novo Status</Label>
         <FormikReactSelect
            name = "status"
            id = "status"
            value = { this.state.status }
            isMulti = { false }
            options = { status_list }
             onChange={(e,v) => {this.setState({status:v})}}
             onBlur={e => {}}
                        
          />
          <br/>
          <Label className="mt-4">Observação</Label>
          <Input
            type="textarea"
            defaultValue={this.state.description}
            onChange={event => {
              this.setState({ description: event.target.value });
            }}
          />
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" outline onClick={toggleModal}>
            Cancelar
          </Button>
          <Button color="primary" onClick={() => this.addChangeStatus()}>
            Confirmar
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default ChangeReservationStatusModal;
