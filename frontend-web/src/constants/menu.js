const data = [{
    id: "properties",
    icon: "simple-icon-home",
    label: "Imoveis",
    to: "/app/property",
    subs: [{
        icon: "iconsminds-file-edit",
        label: "Cadastro",
        to: "/app/property/register"
      },
      {
        icon: "iconsminds-blinklist",
        label: "Listar",
        to: "/app/property/list"
      }

    ]
  },
  {
    id: "curtomers",
    icon: "simple-icon-user",
    label: "Clientes",
    to: "/app/customer",
    subs: [{
        icon: "iconsminds-file-edit",
        label: "Cadastro",
        to: "/app/customer/register"
      },
      {
        icon: "iconsminds-blinklist",
        label: "Listar",
        to: "/app/customer/list"
      }


    ]
  },

  {
    id: "reservations",
    icon: "iconsminds-calendar-1",
    label: "Reservas",
    to: "/app/reservation",
    subs: [{
        icon: "iconsminds-file-edit",
        label: "Cadastro",
        to: "/app/reservation/register"
      },
      {
        icon: "iconsminds-blinklist",
        label: "Listar",
        to: "/app/reservation/list"
      },
      {
        icon: "iconsminds-calendar-4",
        label: "Calendario",
        to: "/app/reservation/calendar"
      }
    ]
  }
  
  ,

  {
    id: "settings",
    icon: "iconsminds-security-settings",
    label: "Configurações",
    to: "/app/settings",
    subs: [
    ]
  }




];
export default data;
