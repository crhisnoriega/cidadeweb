/* Gogo Language Texts

Table of Contents

01.General
02.User Login, Logout, Register
03.Menu
04.Error Page
*/


module.exports = {
  /* 01.General */
  "general.copyright": "Entre Mares",

  /* 02.User Login, Logout, Register */
  "user.login-title": " ",
  "user.register": "Registro",
  "user.forgot-password": "Esqueceu sua senha",
  "user.email": "E-mail",
  "user.login": "Login",
  "user.password": "Senha",
  "user.forgot-password-question": "Esqueceu sua senha?",
  "user.fullname": "Nome Completo",
  "user.login-button": "ENTRAR",
  "user.register-button": "REGISTRAR",
  "user.reset-password-button": "RESETAR",
  "user.buy": "BUY",
  "user.username": "Username",

  /* 03.Menu */
  "menu.app": "Inicio",
  "menu.dashboards": "Dashboards",
  "menu.gogo": "Gogo",
  "menu.start": "Start",
  "menu.second-menu": "Second Menu",
  "menu.second": "Second",
  "menu.ui": "UI",
  "menu.charts": "Charts",
  "menu.chat": "Chat",
  "menu.survey": "Survey",
  "menu.todo": "Todo",
  "menu.search" :"Search",
  "menu.docs": "Docs",
  "menu.blank-page": "Blank Page",
  
  
  "menu.property": "Imovel",
  "menu.register": "Registro",
  
  

 /* 04.Error Page */
 "pages.error-title": "Ooops... looks like an error occurred!",
 "pages.error-code": "Error code",
 "pages.go-back-home": "GO BACK HOME",
 
 "pages.orderby": "Ordenar por: ",
 
 
 "forms.register.property.codigo":"Codigo do Imovel",
 
 "menu.reservation":"Reservas",
 "menu.calendar":"Calendario",
 "menu.customer":"Clientes  ",
 "menu.list":"Lista"

};
